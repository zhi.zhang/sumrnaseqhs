from .sumrnaseqhs import ConfigFile,Summary,Bashrnaseq,__version__
import sys
from time import time, ctime
import datetime
import os

USAGE=r'''
overview
=========
This programm is for multiple RNAseq analysis and creating summary file for all RNAseq samples.

pre-requirment
--------------
1. installed rnaseqhs-0.2 in the $PATH.

usage
-----
1. use as command line::
>>> sumrnaseqhs <CONFIG.INI>

2. use as module::
>>> import sumrnaseqhs

example of CONFIG.INI
---------------------
[SETTINGS]
indir: /work/projects/boehringer/in/
outdir: /work/projects/boehringer/out/
summarydir: /work/projects/boehringer/summary2/
phred: 33
qccheck: true
trim: true
lastkeep: 52
rmadapt: true
ladapter: AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
radapter: None  
overlap: 6
minlen: 25
removeN: true
Ncutoff: 0.1
filtQ: true
minQ: 20
pminQ: 80
qcStat: true
mapping: true
hisat2index: ./rnaseqhs/genome/refrence/HISAT2index/chrX_tran
orientations: fr
rnastrandness: unstranded
gtf: ./rnaseqhs/genome/refrence/HISAT2index/chrX.gtf
drawCover: true
genomebed: /home/users/zzhang/humanGenomeBed/genome.bed
windowsize: 50000
[GROUPS]
L1: 433_1,433_2  
L2: 433_21,433_22
'''

def main1():
    def main(configfile):
        ''' main function of program'''
        config_settings=ConfigFile(configfile).readConfig()[0]
        config_groups=ConfigFile(configfile).readConfig()[1]
        if not os.path.exists(config_settings['summarydir']):
                os.mkdir(config_settings['summarydir'])
        fh=open(os.path.join(config_settings['summarydir'],'main.login'),'a')
        fh.write("----------- %s Starting rnaseqhs program -----------" %ctime(time())+'\n' )
        timeStart = datetime.datetime.now()
        rnaseq_bash=Bashrnaseq(config_settings)
        rnaseq_bash.writeBash()
        rnaseq_bash.runBash()
        timeEnd = datetime.datetime.now()
        fh.write("----------- %s - Finished rnaseqhs program -----------" %ctime(time())+'\n' )
        timeDiff = (timeEnd - timeStart)
        fh.write( "----------- rnaseq program duration %f hours. -----------" %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 )+'\n')
    
        fh.write("----------- %s Starting summary program -----------" %ctime(time())+'\n' )
        timeStart = datetime.datetime.now()    
        t=Summary(config_settings,config_groups)
        t.summaryQC()
        t.summaryDataCleaning()
        t.summaryAlignment()
        t.summaryCoverage()
        t.createAssembly()
        timeEnd = datetime.datetime.now()
        fh.write("----------- %s - Finished summary program -----------" %ctime(time())+'\n' )
        timeDiff = (timeEnd - timeStart)
        fh.write( "----------- summary program duration %f hours. -----------" %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 )+'\n')
        fh.close()
        
    
    if len(sys.argv)>1 and sys.argv[1] != '-h':
        print ("=========== %s Starting main program ===========" %ctime(time()) )
        timeStart = datetime.datetime.now()
        main(sys.argv[1])
        timeEnd = datetime.datetime.now()
        print ("=========== %s - Finished main program ===========" %ctime(time()) )
        timeDiff = (timeEnd - timeStart)
        print ( "=========== Main program duration %f hours. ===========" %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 ))
    elif len(sys.argv)>1 and sys.argv[1] == '-h':
        print ('version %s'%__version__)
        print ('USAGE: sumrnaseqhs <CONFIGFILE.INI>')
        print (USAGE )
    else:
        print ('version %s'%__version__)
        print ('USAGE: sumrnaseqhs <CONFIGFILE.INI>')
        print ('sumrnaseqhs -h for help')
        sys.exit()  

#!/usr/bin/python

r'''
overview
=========
This programm is for multiple RNAseq analysis and creating summary file for all RNAseq samples.

pre-requirment
--------------
1. installed rnaseqhs-<version> in the $PATH.
2. installed parallel in the $PATH.

usage
-----
1. use as command line::
>>> sumrnaseqhs_parallel <CONFIG.INI>

2. use as module::
>>> import sumrnaseqhs

example of CONFIG.INI
---------------------
[SETTINGS]
indir: /work/projects/boehringer/in/
outdir: /work/projects/boehringer/out/
summarydir: /work/projects/boehringer/summary2/
phred: 33
qccheck: true
trim: true
lastkeep: 52
rmadapt: true
ladapter: AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
radapter: None  
overlap: 6
minlen: 25
removeN: true
Ncutoff: 0.1
filtQ: true
minQ: 20
pminQ: 80
qcStat: true
mapping: true
hisat2index: ./rnaseqhs/genome/refrence/HISAT2index/chrX_tran
orientations: fr
rnastrandness: unstranded
gtf: ./rnaseqhs/genome/refrence/HISAT2index/chrX.gtf
drawCover: true
genomebed: /home/users/zzhang/humanGenomeBed/genome.bed
windowsize: 50000
[GROUPS]
L1: 433_1,433_2  
L2: 433_21,433_22
'''

import os
from os import environ
from time import time, ctime
import datetime
import os.path
import sys
import re 
import glob
import subprocess 
import shutil
import configparser
import collections
import csv
from sumrnaseqhs import __version__
import pkg_resources

prepDE=pkg_resources.resource_filename('sumrnaseqhs','prepDE.py')

Q20_Q30_pattern=re.compile(r'''Minimum\sQuality:\s(\d+)
Total\sreads\t(\d+)
Total\sbases\t(\d{1,3}(,\d{3})*)
GC%\t(\d*\.?\d*)
Q20\sbases\t(\d{1,3}(,\d{3})*)
Q20\sbases%\t(\d*\.?\d*)
Q30\sbases\t(\d{1,3}(,\d{3})*)
Q30\sbases%\t(\d*\.?\d*)\n''')

filtlogQ_pattern=re.compile(r'''Quality\scut-off:\s(\d+)
Minimum\spercentage:\s(\d+)
Input:\s(\d+)\sreads\.
Output:\s(\d+)\sreads\.
discarded\s(\d+)\s\((\d+%)\)\slow-quality\sreads\.\n''')

removed_N_pattern=re.compile(r'''Total\sReads\tRemoved\sN\sReads\t%Removed\sN\sRate
(\d+)\t(\d+)\t(\d*\.?\d*)\n''')

sumAlignment_pattern=re.compile(r'''(\d+)\sreads;\sof\sthese:
\s+(\d+)\s\((\d*\.?\d*%)\)\swere\sunpaired;\sof\sthese:
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\s\d+\stimes
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\sexactly\s\d+\stime
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\s>\d+\stimes
(\d*\.?\d*%)\soverall\salignment\srate\n''')
 
sumAlignment_pattern2=re.compile(r'''(\d+)\sreads;\sof\sthese:
\s+(\d+)\s\((\d*\.?\d*%)\)\swere\spaired;\sof\sthese:
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\sconcordantly\s\d+\stimes
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\sconcordantly\sexactly\s\d+\stime
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\sconcordantly\s>\d+\stimes
\s+----
\s+\d+\spairs\saligned\sconcordantly\s\d+\stimes;\sof\sthese:
\s+(\d+)\s\((\d*\.?\d*%)\)\saligned\sdiscordantly\s\d+\stime
\s+----
\s+\d+\spairs\saligned\s\d+\stimes\sconcordantly\sor\sdiscordantly;\sof\sthese:
\s+\d+\smates\smake\sup\sthe\spairs;\sof\sthese:
\s+\d+\s\(\d*\.?\d*%\)\saligned\s\d+\stimes
\s+\d+\s\(\d*\.?\d*%\)\saligned\sexactly\s\d+\stime
\s+\d+\s\(\d*\.?\d*%\)\saligned\s>\d+\stimes
(\d*\.?\d*%)\soverall\salignment\srate\n''')

def getFile(path, regex):
    pattern='%s/*%s'%(path,regex)
    files = glob.glob(pattern)
    return files

def mkDir(path):
    if not os.path.exists(path):
        os.mkdir(path)

def readAvailableNodes( nodefilename ):
    """Return a dict of node names
    with their respective amount of cores."""
    # read file
    f = open( nodefilename )
    cores = f.read().split()
    f.close()
    # count unique entries (that is nodes)
    # to obtain available cores
    nodes = {}
    for node in list(set(cores)):
        nodes[node] = cores.count(node)
    # finish
    return nodes

def coreDistribution(nodes):
    coreDistribution = ""
    if len(nodes.keys()) > 1:
        # write nodes and number of cores into sshloginfile
        sshloginfilename = "/tmp/SSHloginFile.%i" %int(environ['OAR_JOBID'])
        fSSHloginfile = open( sshloginfilename, 'w' )
        for k in nodes.keys():
            fSSHloginfile.write("%i/oarsh %s\n" %(nodes[k], k))
        fSSHloginfile.close()
        coreDistribution = "--sshloginfile %s" %sshloginfilename
    else:
        coreDistribution =  "-j %i" %nodes[ list(nodes.keys())[0] ]
    return coreDistribution

class Summary(object):
    '''creating summary for multiple rnaseq samples. ''' 
    def __init__(self,config_settings,config_groups):
        self.config_settings=config_settings
        self.config_groups=config_groups
        self.basepath=os.path.dirname(config_settings['outdir'])
        self.inbasepath=config_settings['indir']
        self.outbasepath=config_settings['outdir']
        self.dirnames_short=[]
        self.indirnames_long=[]
        self.outdirnames_long=[]
        self.summarydir=config_settings['summarydir']
        if not os.path.exists(self.summarydir):
            os.mkdir(self.summarydir)
        for (dirpath, dirnames, filenames) in os.walk(self.outbasepath):
            dirnames.sort()
            for i in dirnames:
                self.dirnames_short.append(i)
                self.indirnames_long.append(os.path.join(self.inbasepath,i))
                self.outdirnames_long.append(os.path.join(self.outbasepath,i))
            break
        self.dirnames_short.sort()
        self.indirnames_long.sort()
        self.outdirnames_long.sort()
        
        self.nodes=readAvailableNodes( environ['OAR_NODEFILE'] )
        self.coreDistribution=coreDistribution(self.nodes)
        
    def summaryQC(self):
        self.pathQC=os.path.join(self.summarydir,'01.QC')
#         print self.pathQC
        if not os.path.exists(self.pathQC):
            os.mkdir(self.pathQC)
        fh_summaryQC=open(os.path.join(self.pathQC,'summaryQC.txt'),'w')
        fh_summaryQC.write(''.join(['sample_name\t','filtQ_Minimum_Quality\t','filtQ_Total_reads\t','filtQ_Total_bases\t','filtQ_GC%\t','filtQ_Q20_bases\t','filtQ_Q20_bases%\t','filtQ_Q30_bases\t','filtQ_Q30_bases%\t','trimmed_Minimum_Quality\t','trimmed_Total_reads\t','trimmed_Total_bases\t','trimmed_GC%\t','trimmed_Q20_bases\t','trimmed_Q20_bases%\t','trimmed_Q30_bases\t','trimmed_Q30_bases%\n']))  
        for dirname in self.outdirnames_long:
            htmlQC=glob.glob(os.path.join(dirname,'01.QC','*.html'))
#             print htmlQC
            txtQC_Q20_Q30_filtQ=glob.glob(os.path.join(dirname,'01.QC','Q20_Q30_filtQ*.txt'))[0]
            txtQC_Q20_Q30_trimmed=glob.glob(os.path.join(dirname,'01.QC','Q20_Q30_trimmed*.txt'))[0]
#             print txtQC
            for i in htmlQC:
                shutil.copy(i, self.pathQC)
            with open(txtQC_Q20_Q30_filtQ) as f1:
                content0=''.join(f1.readlines())
            match_Q20_Q30_filtQ=re.search(Q20_Q30_pattern,content0).groups()
            with open(txtQC_Q20_Q30_trimmed) as f2:
                content1=''.join(f2.readlines())
            match_Q20_Q30_trimmed=re.search(Q20_Q30_pattern,content1).groups()
            fh_summaryQC.write(''.join([dirname,'\t',match_Q20_Q30_filtQ[0],'\t',match_Q20_Q30_filtQ[1],'\t',match_Q20_Q30_filtQ[2],'\t',match_Q20_Q30_filtQ[4],'\t',match_Q20_Q30_filtQ[5],'\t',match_Q20_Q30_filtQ[7],'\t',match_Q20_Q30_filtQ[8],'\t',match_Q20_Q30_filtQ[10],'\t',match_Q20_Q30_trimmed[0],'\t',match_Q20_Q30_trimmed[1],'\t',match_Q20_Q30_trimmed[2],'\t',match_Q20_Q30_trimmed[4],'\t',match_Q20_Q30_trimmed[5],'\t',match_Q20_Q30_trimmed[7],'\t',match_Q20_Q30_trimmed[8],'\t',match_Q20_Q30_trimmed[10],'\n']))
            if len(glob.glob(os.path.join(dirname,'01.QC','Q20_Q30_filtQ*.txt')))==2 and len(glob.glob(os.path.join(dirname,'01.QC','Q20_Q30_trimmed*.txt')))==2:
                txtQC_Q20_Q30_filtQ=glob.glob(os.path.join(dirname,'01.QC','Q20_Q30_filtQ*.txt'))[1]
                txtQC_Q20_Q30_trimmed=glob.glob(os.path.join(dirname,'01.QC','Q20_Q30_trimmed*.txt'))[1]
                with open(txtQC_Q20_Q30_filtQ) as f1:
                    content0=''.join(f1.readlines())
                match_Q20_Q30_filtQ=re.search(Q20_Q30_pattern,content0).groups()
                with open(txtQC_Q20_Q30_trimmed) as f2:
                    content1=''.join(f2.readlines())
                match_Q20_Q30_trimmed=re.search(Q20_Q30_pattern,content1).groups()
                fh_summaryQC.write(''.join([dirname,'\t',match_Q20_Q30_filtQ[0],'\t',match_Q20_Q30_filtQ[1],'\t',match_Q20_Q30_filtQ[2],'\t',match_Q20_Q30_filtQ[4],'\t',match_Q20_Q30_filtQ[5],'\t',match_Q20_Q30_filtQ[7],'\t',match_Q20_Q30_filtQ[8],'\t',match_Q20_Q30_filtQ[10],'\t',match_Q20_Q30_trimmed[0],'\t',match_Q20_Q30_trimmed[1],'\t',match_Q20_Q30_trimmed[2],'\t',match_Q20_Q30_trimmed[4],'\t',match_Q20_Q30_trimmed[5],'\t',match_Q20_Q30_trimmed[7],'\t',match_Q20_Q30_trimmed[8],'\t',match_Q20_Q30_trimmed[10],'\n']))
        fh_summaryQC.close()  
    
    def summaryDataCleaning(self):
        self.pathDataCleaning=os.path.join(self.summarydir,'02.DataCleaning')
        if not os.path.exists(self.pathDataCleaning):
            os.mkdir(self.pathDataCleaning)
        fh_summaryDataCleaning=open(os.path.join(self.pathDataCleaning,'summaryDataCleaning.txt'),'w')
        fh_summaryDataCleaning.write(''.join(['sample_name\t','Total_Reads\t','Removed_N_Reads\t','%Removed_N_Rate\t','Quality_cut-off\t','Minimum_percentage\t','Input_reads\t','Output_reads\t','discarded_reads\t','discarded_rate\n']))  
        for dirname in self.outdirnames_long:
            logDataCleaningremoved_N=glob.glob(os.path.join(dirname,'02.DataCleaning','removed_N*.log'))[0]
            logDataCleaningfiltlogQ=glob.glob(os.path.join(dirname,'02.DataCleaning','filtlogQ*.log'))[0]
            pdfDataCleaning=glob.glob(os.path.join(dirname,'02.DataCleaning','*.pdf'))[0]
            shutil.copy(pdfDataCleaning, os.path.join(self.pathDataCleaning,os.path.basename(dirname)+'_'+os.path.basename(pdfDataCleaning)))
            with open(logDataCleaningfiltlogQ) as f3:
                content0=''.join(f3.readlines())
            match_filtlogQ=re.search(filtlogQ_pattern,content0).groups()
            with open(logDataCleaningremoved_N) as f4:
                content1=''.join(f4.readlines())
            match_removed_N=re.search(removed_N_pattern,content1).groups()
            fh_summaryDataCleaning.write(''.join([dirname,'\t',match_removed_N[0],'\t',match_removed_N[1],'\t',match_removed_N[2],'\t',match_filtlogQ[0],'\t',match_filtlogQ[1],'\t',match_filtlogQ[2],'\t',match_filtlogQ[3],'\t',match_filtlogQ[4],'\t',match_filtlogQ[5],'\n']))  
            if len(glob.glob(os.path.join(dirname,'02.DataCleaning','removed_N*.log')))==2 and len(glob.glob(os.path.join(dirname,'02.DataCleaning','filtlogQ*.log')))==2:
                logDataCleaningremoved_N=glob.glob(os.path.join(dirname,'02.DataCleaning','removed_N*.log'))[1]
                logDataCleaningfiltlogQ=glob.glob(os.path.join(dirname,'02.DataCleaning','filtlogQ*.log'))[1]
                content0=''.join(open(logDataCleaningfiltlogQ).readlines())
                match_filtlogQ=re.search(filtlogQ_pattern,content0).groups()
                content1=''.join(open(logDataCleaningremoved_N).readlines())
                match_removed_N=re.search(removed_N_pattern,content1).groups()
                fh_summaryDataCleaning.write(''.join([dirname,'\t',match_removed_N[0],'\t',match_removed_N[1],'\t',match_removed_N[2],'\t',match_filtlogQ[0],'\t',match_filtlogQ[1],'\t',match_filtlogQ[2],'\t',match_filtlogQ[3],'\t',match_filtlogQ[4],'\t',match_filtlogQ[5],'\n']))  
        fh_summaryDataCleaning.close()
    
    def summaryAlignment(self):
        self.pathAlignment=os.path.join(self.summarydir,'03.Alignment')
        self.pathAlignment_stringtieout=os.path.join(self.summarydir,'03.Alignment/stringtieout')
#         self.pathAlignment_featurecounts=os.path.join(self.summarydir,'03.Alignment/featurecounts')
        if not os.path.exists(self.pathAlignment):
            os.mkdir(self.pathAlignment)
        if not os.path.exists(self.pathAlignment_stringtieout):
            os.mkdir(self.pathAlignment_stringtieout)
#         if not os.path.exists(self.pathAlignment_featurecounts):
#             os.mkdir(self.pathAlignment_featurecounts)
        fh_summaryAlignment=open(os.path.join(self.pathAlignment,'summaryAlignment.txt'),'w')
#         fh_summaryAlignment.write(''.join(['sample_name\t','Input_Reads\t','Mapped_Reads\t','%Mapped_Rate\t','multiple_alignments_Reads\t','%multiple_alignments_Rates\n']))  
        k=0
        for dirname in self.outdirnames_long:
            txtsumAlignment=glob.glob(os.path.join(dirname,'03.Alignment','alignment.alnstats'))[0]
#             txtfcAlignment=glob.glob(os.path.join(dirname,'03.Alignment','alignment.features_count.txt'))[0]
            transcriptsAlignment=glob.glob(os.path.join(dirname,'03.Alignment','alignment.gtf'))[0]
            shutil.copy(transcriptsAlignment,os.path.join(self.pathAlignment_stringtieout,os.path.basename(dirname)+'_'+os.path.basename(transcriptsAlignment)))
#             shutil.copy(txtfcAlignment,os.path.join(self.pathAlignment_featurecounts,os.path.basename(dirname)+'_'+os.path.basename(txtfcAlignment)))
            with open(txtsumAlignment) as f5:
                content0=''.join(f5.readlines())
            
            if re.search(sumAlignment_pattern,content0):
                if k==0:
                    fh_summaryAlignment.write(''.join(['sample_name\t','Total_Reads\t','Unpaired_Reads\t','Unpaired__Rate\t','0_alignments_Reads\t','0_alignments__Rates\t','1_alignments_Reads\t','1_alignments__Rates\t','>1_alignments_Reads\t','>1_alignments__Rates\t','overall_alignments__Rates\n']))  
                    k=1
                match_sumAlignment=re.search(sumAlignment_pattern,content0).groups()
                fh_summaryAlignment.write(''.join([dirname,'\t',match_sumAlignment[0],'\t',match_sumAlignment[1],'\t',match_sumAlignment[2],'\t',match_sumAlignment[3],'\t',match_sumAlignment[4],'\t',match_sumAlignment[5],'\t',match_sumAlignment[6],'\t',match_sumAlignment[7],'\t',match_sumAlignment[8],'\t',match_sumAlignment[9],'\n']))
            elif re.search(sumAlignment_pattern2,content0):
                if k==0:
                    fh_summaryAlignment.write(''.join(['sample_name\t','Total_Reads\t','paired_Reads\t','paired_Rate\t','0_alignments_concord_Reads\t','0_alignments_concord_Rates\t','1_alignments_concord_Reads\t','1_alignments_concord_Rates\t','>1_alignments_concord_Reads\t','>1_alignments_concord_Rates\t','1_alignments_disconcord_Reads\t','1_alignments_disconcord_Rates\t','overall_alignments__Rates\n']))  
                    k=1
                match_sumAlignment=re.search(sumAlignment_pattern2,content0).groups()
                fh_summaryAlignment.write(''.join([dirname,'\t',match_sumAlignment[0],'\t',match_sumAlignment[1],'\t',match_sumAlignment[2],'\t',match_sumAlignment[3],'\t',match_sumAlignment[4],'\t',match_sumAlignment[5],'\t',match_sumAlignment[6],'\t',match_sumAlignment[7],'\t',match_sumAlignment[8],'\t',match_sumAlignment[9],'\t',match_sumAlignment[10],'\t',match_sumAlignment[11],'\n']))
#                 fh_summaryAlignment.write(''.join([dirname,'\t',match_sumAlignment[5],'\t',match_sumAlignment[6],'\t',match_sumAlignment[7],'\t',match_sumAlignment[8],'\t',match_sumAlignment[9],'\n']))
            else:
                pass
        fh_summaryAlignment.close()
    
    def summaryCoverage(self):
        self.pathCoverage=os.path.join(self.summarydir,'04.Coverage')
        os.mkdir(self.pathCoverage)
        for dirname in self.outdirnames_long:
            pdfCoverage=glob.glob(os.path.join(dirname,'04.Coverage','*.pdf'))[0]
            shutil.copy(pdfCoverage,os.path.join(self.pathCoverage,os.path.basename(dirname)+'_'+os.path.basename(pdfCoverage)))
    
    def createAssembly(self):
        self.pathStringtie=os.path.join(self.summarydir,'05.Stringtie')
        os.mkdir(self.pathStringtie)
        self.pathStringtie_stringtieout=os.path.join(self.summarydir,'05.Stringtie/stringtieout')
        os.mkdir(self.pathStringtie_stringtieout)
#         self.pathStringtie_featurecounts=os.path.join(self.summarydir,'05.Stringtie/featurecounts')
#         os.mkdir(self.pathStringtie_featurecounts)
        fh_txtAssembly=open(os.path.join(self.summarydir,'05.Stringtie/assembly.txt'),'w')
        transcriptsAlignment=glob.glob(os.path.join(self.pathAlignment_stringtieout,'*.gtf'))
        for i in transcriptsAlignment:
            fh_txtAssembly.write(i+'\n')
        fh_txtAssembly.close()
        
        subprocess.call(['stringtie --merge  -G %s -o %s  %s'%(self.config_settings['gtf'] ,os.path.join(self.pathStringtie,'merged.gtf') ,os.path.join(self.pathStringtie,'assembly.txt'))],shell=True)        
        self.mergedGtf=glob.glob(os.path.join(self.pathStringtie,'merged.gtf'))[0]
        alignment_bam_files=[]
        self.parallel_stringtie_args_file=os.path.join(self.pathStringtie,'parallel_stringtie_args.txt')
        fh_parallel_stringtie_args_file=open(self.parallel_stringtie_args_file,'w')
        for dirname in self.outdirnames_long:
            alignment_bam_files.append(glob.glob(os.path.join(dirname,'03.Alignment','alignment.bam'))[0])
        
        for i in alignment_bam_files:
            sample_name=os.path.basename(os.path.dirname(os.path.dirname(i)))
            sample_dir=os.path.join(self.pathStringtie_stringtieout,sample_name)
            os.mkdir(sample_dir)
#             stringtie_path=os.path.join(self.summarydir,'05.stringtieout',os.path.basename(os.path.dirname(os.path.dirname(i))))
            fh_parallel_stringtie_args_file.write(' -e -B -p 4  -G '+self.mergedGtf+' -o '+sample_dir+'/'+sample_name+'_alignment.gtf '+i+'\n')
        fh_parallel_stringtie_args_file.close() 
        command = "cat %s | parallel -u %s --colsep ' ' %s {}" %(self.parallel_stringtie_args_file,self.coreDistribution,'stringtie ')
        print ("%s - Starting gnuparallel for stringtie" %ctime(time()) )
        timeStart = datetime.datetime.now()
        os.system(command)
        timeEnd = datetime.datetime.now()
        print ("%s - Finished for stringtie" %ctime(time()) )
        timeDiff = (timeEnd - timeStart)
        print ( "stringtie Duration %f hours." %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 ))
#redo feature count using merged gtf for each sample 
#         for i in alignment_bam_files:
#             sample_name=os.path.basename(os.path.dirname(os.path.dirname(i)))
#             shell_feature_count="featureCounts -a %s -o %s %s \n"%(self.mergedGtf,os.path.join(self.pathStringtie_featurecounts,sample_name+'_features_count.txt'),i)
#             os.system(shell_feature_count)
#                
#         generator_data = []
#         fc_files=glob.glob(os.path.join(self.pathStringtie_featurecounts,'*features_count.txt'))
#         fc_files.sort()
#         fh_summary_fc=open(os.path.join(self.pathStringtie,'sumfeatures_count.txt'),'w')
#         file_h = open(fc_files[0])
#         a_list = []
#         csv_reader = csv.reader(file_h, delimiter='\t')
#         csv_reader.__next__()
#         for row in csv_reader:
#             a_list.append(row[0:6])
#         generator_data.append((n for n in a_list))
#         file_h.close()
#         fc_files.sort()
#         for fc_file in fc_files:
#             file_h = open(fc_file)
#             a_list = []
# #            htseq_file_pattern=re.compile(r'%s(\d+_\d+)_accepted_hits.uniq.sort.htseq_count.txt'%(os.path.join(self.summarydir,'03.Alignment/htseq/')))
# #             fc_file_pattern=re.compile(r'%s(\S+)_alignment\.features_count.txt'%(os.path.join(self.summarydir,'03.Alignment/featurecounts/')))
# #             match_fc_file=re.search(fc_file_pattern,fc_file).groups()[0]
# #             a_list.append(match_fc_file)
#             csv_reader = csv.reader(file_h, delimiter='\t')
#             csv_reader.__next__()
#             for row in csv_reader:
#                 a_list.append(row[6])
#             generator_data.append((n for n in a_list))
#             file_h.close()
#         
#         csv_writer = csv.writer(fh_summary_fc, delimiter='\t')
#         for row in list(zip(*generator_data)):
#             csv_writer.writerow(row)
#         fh_summary_fc.close()
        subprocess.call(['python3.6  %s  -i %s -g %s -t %s \n'%(prepDE,self.pathStringtie_stringtieout,os.path.join(self.pathStringtie,'gene_count_matrix.csv'),os.path.join(self.pathStringtie,'transcript_count_matrix.csv'))],shell=True)        
        
class Bashrnaseq(object):  
    '''run rnaseq command for each sample. '''
    def __init__(self,config_settings):
        self.config_settings=config_settings
        self.basepath=os.path.dirname(config_settings['indir'])
        self.inbasepath=config_settings['indir']
        self.outbasepath=config_settings['outdir']
        self.summarydir=config_settings['summarydir']
        if not os.path.exists(self.summarydir):
            os.mkdir(self.summarydir)
        self.dirnames_short=[]
        self.indirnames_long=[]
        self.outdirnames_long=[]
        if not os.path.exists(self.outbasepath):
            os.mkdir(self.outbasepath)
        for (dirpath, dirnames, filenames) in os.walk(self.inbasepath):
            for i in dirnames:
                self.dirnames_short.append(i)
                self.indirnames_long.append(os.path.join(self.inbasepath,i))
                self.outdirnames_long.append(os.path.join(self.outbasepath,i))
                if not os.path.exists(os.path.join(self.outbasepath,i)):
                    os.mkdir(os.path.join(self.outbasepath,i))
            break
        self.dirnames_short.sort()
        self.indirnames_long.sort()
        self.outdirnames_long.sort()
        self.nodes=readAvailableNodes( environ['OAR_NODEFILE'] )
        self.coreDistribution=coreDistribution(self.nodes)
             
    def writeBash(self):
        self.bashfile=[]
        self.parallel_rnaseq_args_file=os.path.join(self.summarydir,'parallel_rnaseq_args.txt')
        fh_parallel_rnaseq_args_file=open(self.parallel_rnaseq_args_file,'w')
        for dirname in self.dirnames_short:
            fh_bash=open(os.path.join(self.outbasepath,dirname)+'.sh','w')
            self.bashfile.append(os.path.join(self.outbasepath,dirname)+'.sh')
            para=''
            for (i,j) in self.config_settings.items():
                if i=='indir':
                    para=para+' --'+i+' '+os.path.join(j,dirname)+' '
                elif i=='outdir':
                    para=para+' --'+i+' '+os.path.join(j,dirname)+' '
                elif i=='summarydir':
                    pass
                else:
                    para=para+' --'+str(i)+' '+str(j)+' '
                     
            bash_content='''
            #!/bin/bash -login 
            module use $RESIF_ROOTINSTALL/lcsb/modules/all 
            module load lang/Java/1.7.0_21 
            module load lang/R/3.0.2-goolf-1.4.10 
            #module load bio/SAMtools/1.2-goolf-1.4.10 
            module load bio/BWA/0.7.12-goolf-1.4.10 
            #module load bio/BEDTools/2.23.0-goolf-1.4.10 
            rnaseqhs %s 
            '''%(para)
            fh_bash.write(bash_content)
            fh_bash.close()
            fh_parallel_rnaseq_args_file.write(para+'\n')
        fh_parallel_rnaseq_args_file.close()  
          
    def runBash(self):
        command = "cat %s | parallel -u %s --colsep ' ' %s {}" %(self.parallel_rnaseq_args_file,self.coreDistribution,'rnaseqhs')
        print ("%s - Starting gnuparallel for rnaseqhs" %ctime(time()) )
        timeStart = datetime.datetime.now()
        os.system(command)
        timeEnd = datetime.datetime.now()
        print ("%s - Finished for rnaseqhs" %ctime(time()) )
        timeDiff = (timeEnd - timeStart)
        print ( "rnaseqhs Duration %f hours." %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 ))

class ConfigFile(object):
    '''read config.ini file class. '''
    def __init__(self,configfile):
        self.configfile=configfile

    def readConfig(self):
        Config=configparser.ConfigParser()
        Config.optionxform = str
        Config.read(self.configfile)
        tmplist=[]
        settings=Config.items(Config.sections()[0])
        groups=Config.items(Config.sections()[1]) 
        for i in settings:
            tmplist.append((i[0],i[1]))
        config_settings=collections.OrderedDict(tmplist)
        tmplist=[]
        for i in groups:
            tmplist.append((i[0].upper(),i[1]))
        config_groups=collections.OrderedDict(tmplist)
        return config_settings,config_groups    
                
def main(configfile):
    ''' main function of program'''
    config_settings=ConfigFile(configfile).readConfig()[0]
    config_groups=ConfigFile(configfile).readConfig()[1]
    if not os.path.exists(config_settings['summarydir']):
            os.mkdir(config_settings['summarydir'])
    fh=open(os.path.join(config_settings['summarydir'],'main.login'),'w')
    fh.write("----------- %s Starting rnaseq program -----------" %ctime(time())+'\n' )
    timeStart = datetime.datetime.now()
    rnaseq_bash=Bashrnaseq(config_settings)
    rnaseq_bash.writeBash()
    rnaseq_bash.runBash()
    timeEnd = datetime.datetime.now()
    fh.write("----------- %s - Finished rnaseq program -----------" %ctime(time())+'\n' )
    timeDiff = (timeEnd - timeStart)
    fh.write( "----------- rnaseq program duration %f hours. -----------" %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 )+'\n')

    fh.write("----------- %s Starting summary program -----------" %ctime(time())+'\n' )
    timeStart = datetime.datetime.now()    
    t=Summary(config_settings,config_groups)
    t.summaryQC()
    t.summaryDataCleaning()
    t.summaryAlignment()
    t.summaryCoverage()
    t.createAssembly()
    timeEnd = datetime.datetime.now()
    fh.write("----------- %s - Finished summary program -----------" %ctime(time())+'\n' )
    timeDiff = (timeEnd - timeStart)
    fh.write( "----------- summary program duration %f hours. -----------" %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 )+'\n')
    fh.close()
    
if __name__=='__main__' :
    if len(sys.argv)>1:
        print ("=========== %s Starting main program ===========" %ctime(time()) )
        timeStart = datetime.datetime.now()
        main(sys.argv[1])
        timeEnd = datetime.datetime.now()
        print ("=========== %s - Finished main program ===========" %ctime(time()) )
        timeDiff = (timeEnd - timeStart)
        print ( "=========== Main program duration %f hours. ===========" %(((timeDiff.microseconds + (timeDiff.seconds + timeDiff.days * 24. * 3600.) * 10**6)/ 10**6)/3600 ))
    else:
        print ('version %s'%__version__)
        print ('USAGE: sumrnaseq_parallel <CONFIGFILE.INI>')
        sys.exit()  

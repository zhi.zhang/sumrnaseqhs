from setuptools import setup, find_packages

PACKAGE = "sumrnaseqhs"
NAME = "sumrnaseqhs"
DESCRIPTION = "This programm is for multiple RNAseq analysis and creating summary file for all RNAseq samples."
AUTHOR = 'Zhi Zhang'
AUTHOR_EMAIL = 'zzjerryzhang@gmail.com'
URL = 'https://git-r3lab.uni.lu/zhi.zhang/sumrnaseqhs'
download_url = 'https://git-r3lab.uni.lu/zhi.zhang/rnaseqhs/raw/master/sumrnaseqhs/dist/sumrnaseqhs-0.1.tar.gz'
VERSION = __import__(PACKAGE).__version__

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name=NAME,
#      packages=['rnaseq'],
      version=VERSION,
      description=DESCRIPTION,
      long_description=readme(),
      classifiers=[
              'Development Status :: 3 - Alpha',
	           'License :: OSI Approved :: MIT License',
	           'Programming Language :: Python :: 3.6',
                'Topic :: Scientific/Engineering :: Bio-Informatics',
	           ],
      keywords='sumrnaseqhs main',
      url=URL,
      download_url =download_url ,
      author=AUTHOR,
      author_email=AUTHOR_EMAIL,
      test_suite='nose.collector',
      tests_require=['nose==1.3.7','rnaseqhs==0.2'],
      #scripts=['bin/rnaseq'],
      entry_points = {
             'console_scripts': ['sumrnaseqhs_parallel=sumrnaseqhs.command_line_parallel:main1','sumrnaseqhs=sumrnaseqhs.command_line:main1'],             
               },
      license='MIT',
      packages=find_packages(exclude=['tests']),
      #install_requires=['markdown', ],
      #dependency_links=['http://github.com/user/repo/tarball/master#egg=package-1.0']
      include_package_data=True,
      zip_safe=False)


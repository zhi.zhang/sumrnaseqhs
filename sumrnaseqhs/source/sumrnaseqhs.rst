sumrnaseqhs package
===================

Subpackages
-----------

.. toctree::

    sumrnaseqhs.tests

Submodules
----------

sumrnaseqhs.command_line module
-------------------------------

.. automodule:: sumrnaseqhs.command_line
    :members:
    :undoc-members:
    :show-inheritance:

sumrnaseqhs.command_line_parallel module
----------------------------------------

.. automodule:: sumrnaseqhs.command_line_parallel
    :members:
    :undoc-members:
    :show-inheritance:

sumrnaseqhs.prepDE module
-------------------------

.. automodule:: sumrnaseqhs.prepDE
    :members:
    :undoc-members:
    :show-inheritance:

sumrnaseqhs.sumrnaseqhs module
------------------------------

.. automodule:: sumrnaseqhs.sumrnaseqhs
    :members:
    :undoc-members:
    :show-inheritance:

sumrnaseqhs.sumrnaseqhs_parallel module
---------------------------------------

.. automodule:: sumrnaseqhs.sumrnaseqhs_parallel
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: sumrnaseqhs
    :members:
    :undoc-members:
    :show-inheritance:

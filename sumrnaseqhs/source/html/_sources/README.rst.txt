sumrnaseqhs
===========

overview
=========
This programm is for multiple RNAseq analysis and creating summary file for all RNAseq samples.

pre-requirment
--------------
1. installed rnaseqhs-<version> in the $PATH.

Installation
-------------
1. tar zxvf sumrnaseqhs-<version>.tar.gz
2. cd sumrnaseqhs-<version>
3. python setup.py install --user


Usage
-----
1. use as command line::
    >>> sumrnaseqhs <CONFIG.INI>

2. use as module::
    >>> import sumrnaseqhs

3.example of CONFIG.INI::
-------------------------
|    [SETTINGS]
|    indir: /work/projects/boehringer/in/
|    outdir: /work/projects/boehringer/out/
|    summarydir: /work/projects/boehringer/summary2/
|    phred: 33
|    qccheck: true
|    trim: true
|    lastkeep: 52
|    rmadapt: true
|    ladapter: AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
|    radapter: None  
|    overlap: 6
|    minlen: 25
|    removeN: true
|    Ncutoff: 0.1
|    filtQ: true
|    minQ: 20
|    pminQ: 80
|    qcStat: true
|    mapping: true
|    hisat2index: ./rnaseqhs/genome/refrence/HISAT2index/chrX_tran
|    orientations: fr
|    rnastrandness: unstranded
|    gtf: ./rnaseqhs/genome/refrence/HISAT2index/chrX.gtf
|    drawCover: true
|    genomebed: /home/users/zzhang/humanGenomeBed/genome.bed
|    windowsize: 50000
|    [GROUPS]
|    L1: 433_1,433_2  
|    L2: 433_21,433_22


Links
-----

* `Project homepage <https://git-r3lab.uni.lu/zhi.zhang/sumrnaseqhs>`_
* `Github page <https://git-r3lab.uni.lu/zhi.zhang/sumrnaseqhs>`_
    written by Zhi Zhang.